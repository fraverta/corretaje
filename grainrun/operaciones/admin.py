from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Operacion,Liquidacion,LiquidacionAPrecio,LiquidacionAFijar

# Register your models here.


class OperacionAdmin(admin.ModelAdmin):
    #list all fields from Productor in admin model
    list_display = [field.attname for field in Operacion._meta.fields]

class LiquidacionAdmin(admin.ModelAdmin):
    #list all fields from Productor in admin model
    list_display = [field.attname for field in Liquidacion._meta.fields]


class LiquidacionAPrecioAdmin(admin.ModelAdmin):
    #list all fields from Productor in admin model
    list_display = [field.attname for field in LiquidacionAPrecio._meta.fields]

class LiquidacionAFijarAdmin(admin.ModelAdmin):
    #list all fields from Productor in admin model
    list_display = [field.attname for field in LiquidacionAFijar._meta.fields]


admin.site.register(Operacion, OperacionAdmin)
admin.site.register(LiquidacionAPrecio, LiquidacionAPrecioAdmin)
admin.site.register(LiquidacionAFijar, LiquidacionAFijarAdmin)
