from django import forms
from .models import Operacion, LiquidacionAFijar, LiquidacionAPrecio, STATUS_LIQ_A_FIJAR_PP, STATUS_LIQ_A_PRECIO_PP, STATUS_LIQ_A_FIJAR_PFP
from productores.models import Productor
from functools import partial
from django.core.validators import ValidationError
DateInput = partial(forms.DateInput, {'class': 'datepicker'})

class OperacionForm(forms.ModelForm):
    fecha_inicio = forms.DateField(widget=DateInput(), label='Fecha de Inicio')
    fecha_fin = forms.DateField(widget=DateInput(), label='Fecha de Fin')
    class Meta:
        model = Operacion
        exclude = ['aproved']

    def __init__(self, *args, **kwargs):
        super(forms.ModelForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if self.initial is not None and 'restrict_prod_to' in self.initial.keys():
            self.fields['productor'].queryset = Productor.objects.filter(agente=self.initial['restrict_prod_to'])
        self.fields['productor'].queryset = self.fields['productor'].queryset.order_by('apellido', 'nombre')
        if self.initial is not None and 'readonly' in self.initial.keys():
            for f in self.fields.values():
                f.widget.attrs['readonly'] = self.initial['readonly']
                f.widget.attrs['disabled'] = self.initial['readonly']
        if instance is not None and instance.pk:
            self.fields['op_tipo'].required = False
            self.fields['op_tipo'].widget.attrs['disabled'] = True
            self.fields['moneda'].required = False
            self.fields['moneda'].widget.attrs['disabled'] = True
            if instance.op_tipo=='F' and 'precio' in self.fields:
                del self.fields['precio']
        else:
            if 'agente' in self.fields:
                del self.fields['agente']

    def clean_op_tipo(self):
        instance = getattr(self, 'instance', None)
        if instance and instance.pk :
            return instance.op_tipo
        else:
            return self.cleaned_data['op_tipo']


class LiquidacionAPrecioForm(forms.ModelForm):
    contrato_ = forms.CharField(label="Contrato", widget=forms.TextInput(attrs={'readonly':'readonly'}))
    fecha_cambio_estado = forms.DateField(widget=DateInput())
    class Meta:
        model = LiquidacionAPrecio
        exclude = ['contrato']
        fields = ['contrato_','toneladas','estado','fecha_cambio_estado', 'tipo_de_cambio','numero_LPG']

    def __init__(self, *args, **kwargs):
        # Save contrato in form to use in validation (clean method)
        self.contrato = kwargs.pop('contrato', None)

        super(forms.ModelForm, self).__init__(*args, **kwargs)
        data = getattr(self, 'initial', None)


        if data is not None and 'contrato_' in data.keys() and data['contrato_'].moneda == 'ARG':
            del self.fields['tipo_de_cambio']
        else:
            instance = getattr(self, 'instance', None)
            if instance is not None and instance.pk and instance.contrato.moneda == 'ARG':
                del self.fields['tipo_de_cambio']

        if self.initial is not None and 'readonly' in self.initial.keys():
            for f in self.fields.values():
                f.widget.attrs['readonly'] = self.initial['readonly']
                f.widget.attrs['disabled'] = self.initial['readonly']


    def clean(self):
        cd = self.cleaned_data

        estado = cd.get("estado")

        if estado >= STATUS_LIQ_A_PRECIO_PP and self.contrato.moneda == 'USA' \
                and cd.get('tipo_de_cambio') is None:
            raise ValidationError('Para pasar al estado Pendiente de Pago, debe definir el tipo de cambio.',code='invalid')

        return cd


class LiquidacionAFijarForm(forms.ModelForm):
    contrato_ = forms.CharField(label="Contrato", widget=forms.TextInput(attrs={'readonly': 'readonly'}))
    fecha_cambio_estado = forms.DateField(widget=DateInput())

    class Meta:
        model = LiquidacionAFijar
        exclude = ['contrato']
        fields = ['contrato_', 'toneladas', 'precioTonelada', 'estado', 'fecha_cambio_estado', 'tipo_de_cambio', 'numero_LPG']

    def __init__(self, *args, **kwargs):
        #Save contrato in form to use in validation (clean method)
        self.contrato = kwargs.pop('contrato', None)

        super(forms.ModelForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        data = getattr(self, 'initial', None)

        if data is not None and 'contrato_' in data.keys() and data['contrato_'].moneda == 'ARG':
            del self.fields['tipo_de_cambio']
        else:
            instance = getattr(self, 'instance', None)
            if instance is not None and instance.pk and instance.contrato.moneda == 'ARG':
                del self.fields['tipo_de_cambio']

        if self.initial is not None and 'readonly' in self.initial.keys():
            for f in self.fields.values():
                f.widget.attrs['readonly'] = self.initial['readonly']
                f.widget.attrs['disabled'] = self.initial['readonly']

    def clean(self):
        cd = self.cleaned_data

        estado = cd.get("estado")

        if estado > STATUS_LIQ_A_FIJAR_PFP and cd.get('precioTonelada') is None:
            raise ValidationError('Para pasar al estado ' + self.Meta.model.ESTADO_CHOICE[cd.get("estado") - 1][1] + ' debe definir el precio del cereal por tonelada.',
                                  code='invalid')

        if estado >= STATUS_LIQ_A_FIJAR_PP and self.contrato.moneda == 'USA' \
                and cd.get('tipo_de_cambio') is None:
            raise ValidationError('Para pasar al estado Pendiente de Pago, ' + self.Meta.model.ESTADO_CHOICE[cd.get("estado") - 1][1] + ' debe haber definido el tipo de cambio.',
                                  code='invalid')
        return cd

