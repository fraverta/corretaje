import django_tables2 as tables
from .models import Operacion
from django.contrib.humanize.templatetags.humanize import intcomma
from django.utils.html import format_html
from datetime import datetime
from pytz import timezone
import operaciones.models as models

ARGENTINA_TIME = timezone('America/Argentina/Buenos_Aires')


def gen_op_estado_info_html(agente, record):
    status = record.status(agente)
    if status == models.CENDED_WITH_ERRORS:
        return format_html(
            '<span title="Contrato finalizado con inconvenientes"style="padding:1px;color:red;vertical-align: middle;" class="glyphicon glyphicon-remove">')
    elif status == models.CENDED_WITHOUT_ERRORS:
        return format_html(
            '<span title="Contrato finalizado sin inconvenientes." style="padding:1px;color:green;vertical-align: middle;" class="glyphicon glyphicon-ok">')
    elif status == models.CINPROGRESS_WITH_WARNING:
        return format_html(
            '<span title="Contrato en progreso con alarmas."style="padding:1px;color:orange;vertical-align: middle;" class="fa fa-circle">')
    elif status == models.CINPROGRESS_WITHOUT_WARNING:
        return format_html(
            '<span title="Contrato en progreso sin alarmas."style="padding:1px;color:green;vertical-align: middle;" class="fa fa-circle">')
    elif status == models.CENDSOON_WITH_PE:
        return format_html('<span title="Contrato que vence en ' + str((record.fecha_fin - ARGENTINA_TIME.localize(
            datetime.today()).date()).days) + ' días con toneladas pendiente de entrega."style="padding:1px;color:orange;vertical-align: middle;" class="fa fa-circle">')

    elif status == models.CENDED_WITH_WARNING_IN_LIQ:
        return format_html(
            '<span title="Contrato finalizado con inconvenientes. Revise las liquidaciones."style="padding:1px;color:red;vertical-align: middle;" class="glyphicon glyphicon-remove">')
    elif status == models.CENDED_WITH_PE:
        return format_html(
            '<span title="El contrato ha finalizado pero no se han entregado todo el cereal acordado en el contrato."style="padding:1px;color:red;vertical-align: middle;" class="glyphicon glyphicon-remove">')
    elif status == models.CEND_WITH_SURPLUS:
        return format_html(
            '<span title="El contrato ha finalizado y se ha entregado mayor cantidad de cereal que la acordada. Es necesario definir el excedente."style="padding:1px;color:red;vertical-align: middle;" class="glyphicon glyphicon-remove">')
    elif status == models.CINPROGRESS_WITH_WARNING_IN_LIQ:
        return format_html(
            '<span title="Contrato en progreso con alarmas. Revise las liquidaciones."style="padding:1px;color:orange;vertical-align: middle;" class="fa fa-circle">')
    elif status == models.CINPROGRESS_WITH_SURPLUS:
        return format_html(
            '<span title="Contrato en progreso con liquidaciones por mayor cantidad de toneladas que las acordadas."style="padding:1px;color:orange;vertical-align: middle;" class="fa fa-circle">')
    elif status == models.C_NO_APPROVED:
        return format_html(
            '<span title="El contrato espera aprobación."style="padding:1px;color:black;vertical-align: middle;" class="fa fa-spinner fa-spin">')
    else:
        return record.status(agente)



class OperacionTable(tables.Table):
    estado = tables.Column(verbose_name="", orderable=False, empty_values=())
    acciones = tables.TemplateColumn(verbose_name=("Acciones"),template_name='operaciones/action_column_global_view.html',orderable=False)

    def render_precio(self, value, record):
        currency = 'USD '  if record.moneda == 'USA' else '$ '
        return currency + intcomma(round(value, 2))

    def render_estado(self, record):
        return gen_op_estado_info_html(self.agente, record)


    def __init__(self, agente, *args, **kwargs):
        super(tables.Table, self).__init__(*args, **kwargs)
        self.agente = agente

    class Meta:
        model = Operacion
        empty_text = "No hay operaciones que cumplan su criterio de búsqueda."
        attrs = {"class": "table-striped table-bordered"}
        attrs = {'class': 'paleblue'}
        attrs = {'id':'op-table'}
        exclude = ['id', 'aproved']
        sequence = ['estado', '...']
        order_by = ['-fecha_inicio', 'fecha_fin']



class OperacionTable_local_view(tables.Table):
    estado = tables.Column(verbose_name="", orderable=False, empty_values=())
    acciones = tables.TemplateColumn(verbose_name=("Acciones"),
                                     template_name='operaciones/action_column_local_view.html',
                                     orderable=False)


    def render_precio(self, value, record):
        currency = 'USD ' if record.moneda == 'USA' else '$ '
        return currency + intcomma(round(value, 2))

    def render_estado(self, record):
        return gen_op_estado_info_html(self.agente, record)

    def __init__(self, agente, *args, **kwargs):
        super(tables.Table, self).__init__(*args, **kwargs)
        self.agente = agente

    class Meta:
        model = Operacion
        empty_text = "No hay operaciones que cumplan su criterio de búsqueda."
        attrs = {"class": "table-striped table-bordered"}
        attrs = {'class': 'paleblue'}
        attrs = {'id': 'op-table'}
        exclude = ['id','agente', 'aproved']
        sequence = ['estado', '...']