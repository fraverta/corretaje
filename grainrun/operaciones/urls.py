from django.conf.urls import url
from . import views
from django_filters.views import object_filter

urlpatterns = [
    url(r'^operacion/list/$', views.operacion_list, name='operacion_list'),
    url(r'^operacion/new/$', views.operacion_new, name='operacion_new'),
    url(r'^operacion/liquidacion/(?P<operacion_pk>\d+)/new/$', views.liquidacion_new, name='liquidacion_new'),
    url(r'^operacion/(?P<pk>\d+)/edit/$', views.operacion_edit, name='operacion_edit'),
    url(r'^operacion/(?P<pk>\d+)/delete/$', views.operacion_delete, name='operacion_delete'),
    url(r'^operacion/(?P<pk>\d+)/detail/$', views.operacion_detail, name='operacion_detail'),
    url(r'^operacion/liquidacion/P/(?P<pk>\d+)/edit/$', views.liquidacion_edit_a_precio, name='liquidacion_P_edit'),
    url(r'^operacion/liquidacion/F/(?P<pk>\d+)/edit/$', views.liquidacion_edit_a_fijar, name='liquidacion_F_edit'),
    url(r'^operacion/liquidacion/(?P<t>P|F)/(?P<pk>\d+)/delete/$', views.liquidacion_delete, name='liquidacion_delete'),
    url(r'^operacion/liquidacion/(?P<t>P|F)/(?P<pk>\d+)/detail/$', views.liquidacion_detail, name='liquidacion_detail'),

]
