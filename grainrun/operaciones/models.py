from abc import abstractmethod

from django.db import models
from productores.models import Productor
from agentes.models import Agente
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.db.models import Sum
from datetime import datetime, timedelta
from pytz import timezone
from django.core.validators import MinValueValidator, MaxValueValidator
from decimal import *

#Auditlog imports
from auditlog.models import AuditlogHistoryField
from auditlog.registry import auditlog
from django.db import models

ARGENTINA_TIME = timezone('America/Argentina/Buenos_Aires')


# Liquidacion estados identifier
ALARMA_PL = 2
ALARMA_PP = 3
ALARMA_PFP = 4
#Alarmas para mapear con filtros
ALARMA_CONTRATO_PRONTO_A_VENCERSE = 0 #para identicar los id de las alarmas en filters
ALARMA_CONTRATO_VENCIDO_PE = 1 #para identicar los id de las alarmas en filters
ALARMA_CONTRATO_CON_EXCEDENTE = 5 #para identicar los id de las alarmas en filters

L_FINALIZADA = 5
L_NON_ALARMA = 6
ALARMA_PE = 7
CONTRATO_FINALIZADO_PE = 8
CONTRATO_FINALIZADO_PFP = 9
CEND_AND_LIQ_IN_PROGRESS =10 # el contrato finalizo pero la liquidacion sigue en progreso
L_NON_APPROVED = 99


# Contrato status identifier
CENDED_WITH_ERRORS = 1
CENDED_WITHOUT_ERRORS = 2
CINPROGRESS_WITH_WARNING = 3
CINPROGRESS_WITHOUT_WARNING = 4
CENDSOON_WITH_PE = 5 # Contrato finaliza pronto con kilos pendiente de entrega
C_NO_APPROVED = 99

# Nuevos identificadores de estado de los contratos
CENDED_WITH_WARNING_IN_LIQ = 10 # El contrato finalizo y posee warning en las liquidaciones
CENDED_WITH_PE = 11 #El contrato finalizo con kilos pendientes de entrega
CEND_WITH_SURPLUS = 12 #El contrato finalizo con mayor cantidad de kilos entregados que los enunciados en el contrato
CINPROGRESS_WITH_WARNING_IN_LIQ = 13  #Contrato en progreso con warning en las liquidaciones
CINPROGRESS_WITH_SURPLUS = 14 #Contrato en progreso con mayor cantidad de kilos entregados que los enunciados en el contrato


# Estados Liquidacion a fijar
STATUS_LIQ_A_FIJAR_PE  = 1 # Pendiente de Entrega
STATUS_LIQ_A_FIJAR_PFP = 2 # Pendiente de Precio
STATUS_LIQ_A_FIJAR_PL  = 3 # Pendiente de Liquidación
STATUS_LIQ_A_FIJAR_PP  = 4 # Pendiente de Pago
STATUS_LIQ_A_FIJAR_PC  = 5 # Pagado a Cliente

# Estados Liquidacion a precio
STATUS_LIQ_A_PRECIO_PE = 1 #Pendiente de Entrega
STATUS_LIQ_A_PRECIO_PL = 2 #Pendiente de Liquidacion
STATUS_LIQ_A_PRECIO_PP = 3 #Pendiente de Pago
STATUS_LIQ_A_PRECIO_PC = 4 #Pagado a Cliente

class OpAlarmManager(models.Manager):

    '''
    It returns a list of contracts ids that are going to expire soon. It means,
    contracts whose expire date is within X days from today and
    they have tons whose state is "pendiente de entrega".
    '''
    def get_alarm0(self, agente):
        startdate = ARGENTINA_TIME.localize(datetime.today()).date()
        enddate = startdate + timedelta(days=agente.user.userprofile.alarma0_expire_days - 1)
        if agente.is_local_view:
            contratos = super().get_queryset().filter(agente=agente, fecha_fin__range=[startdate, enddate])
        else:
            contratos = super().get_queryset().filter(fecha_fin__range=[startdate, enddate])

        ids_contratos_warning = []
        for c in contratos:
            qs = LiquidacionAPrecio.objects.all() if c.op_tipo == 'P' else LiquidacionAFijar.objects.all()
            totalLiquidadas = (lambda x: x if x is not None else 0)(qs.filter(contrato=c.id, estado__gte=2)
                                                                    .aggregate(Sum('toneladas'))['toneladas__sum'])
            if c.toneladas > totalLiquidadas:
                ids_contratos_warning.append(c.id)
        return ids_contratos_warning

    '''
    It returns a list of contracts ids that have already expired. It means,
    contracts whose expire date is in the past and
    they have tons whose state is "pendiente de entrega".
    '''
    def get_alarm1(self, agente):
        today = ARGENTINA_TIME.localize(datetime.today()).date()
        if agente.is_local_view:
            contratos = super().get_queryset().filter(agente=agente, fecha_fin__lt = today )
        else:
            contratos = super().get_queryset().filter(fecha_fin__lt = today)

        ids_contartos_warning = []
        for c in contratos:
            qs = LiquidacionAPrecio.objects.all() if c.op_tipo == 'P' else LiquidacionAFijar.objects.all()
            totalLiquidadas = (lambda x: x if x is not None else 0)(qs.filter(contrato=c.id, estado__gte=2)
                                                                    .aggregate(Sum('toneladas'))['toneladas__sum'])
            if c.toneladas > totalLiquidadas:
                ids_contartos_warning.append(c.id)
        return ids_contartos_warning

    '''
    It returns a list of contracts ids that have liquidations for more tons
    than the given by the contract
    '''

    def get_alarmCE(self, agente):
        if agente.is_local_view:
            contratos = super().get_queryset().filter(agente=agente)
        else:
            contratos = super().get_queryset()

        ids_contartos_warning = []
        for c in contratos:
            qs = LiquidacionAPrecio.objects.all() if c.op_tipo == 'P' else LiquidacionAFijar.objects.all()
            totalLiquidadas = (lambda x: x if x is not None else 0)(qs.filter(contrato=c.id).aggregate(Sum('toneladas'))['toneladas__sum'])
            if c.toneladas < totalLiquidadas:
                ids_contartos_warning.append(c.id)
        return ids_contartos_warning

    '''
    '''
    def get_alarmPL(self, agente):
        date = ARGENTINA_TIME.localize(datetime.today()).date() - timedelta(days=agente.user.userprofile.alarma_liq_pl)
        if  agente.is_local_view:
            contratos = LiquidacionAPrecio.objects.filter(contrato__agente=agente, estado=2,
                                                       fecha_cambio_estado__lt=date).values('contrato__id')
            contratos = contratos.union(LiquidacionAFijar.objects.filter(contrato__agente=agente, estado=3,
                                                       fecha_cambio_estado__lt=date).values('contrato__id')).distinct()
        else:
            contratos = LiquidacionAPrecio.objects.filter(estado=2, fecha_cambio_estado__lt=date).values('contrato__id')
            contratos = contratos.union(LiquidacionAFijar.objects.filter(estado=3, fecha_cambio_estado__lt=date).values('contrato__id'))\
                        .distinct()

        return contratos

    '''
    '''
    def get_alarmPP(self, agente):
        date = ARGENTINA_TIME.localize(datetime.today()).date() - timedelta(days=agente.user.userprofile.alarma_liq_pp)
        if agente.is_local_view:
            contratos = LiquidacionAPrecio.objects.filter(contrato__agente=agente, estado=3,
                                                          fecha_cambio_estado__lt=date).values('contrato__id')
            contratos = contratos.union(LiquidacionAFijar.objects.filter(contrato__agente=agente, estado=4,
                                                                         fecha_cambio_estado__lt=date).values(
                'contrato__id')).distinct()
        else:
            contratos = LiquidacionAPrecio.objects.filter(estado=3, fecha_cambio_estado__lt=date).values('contrato__id')
            contratos = contratos.union(
                LiquidacionAFijar.objects.filter(estado=4, fecha_cambio_estado__lt=date).values('contrato__id')) \
                .distinct()

        return contratos


    def get_alarmPFP(self, agente):
        date = ARGENTINA_TIME.localize(datetime.today()).date() + timedelta(days=agente.user.userprofile.alarma_liqPFP)
        if agente.is_local_view:
            contratos = LiquidacionAFijar.objects.filter(contrato__agente=agente, estado=STATUS_LIQ_A_FIJAR_PFP,
                                                          contrato__fecha_fin__lt=date).values('contrato__id').distinct()
        else:
            contratos = LiquidacionAFijar.objects.filter(estado=STATUS_LIQ_A_FIJAR_PFP, contrato__fecha_fin__lt=date).values('contrato__id').distinct()

        return contratos


class Operacion(models.Model):
    A_PRECIO_OP = 'P'
    A_FIJAR_OP = 'F'
    OP_CHOICES = [('P', 'A PRECIO'), ('F', 'A FIJAR')]
    CEREAL_CHOICES = [('SJ', 'SOJA'), ('MA', 'MAIZ'), ('TR', 'TRIGO'), ('SR', 'SORGO'), ('GI', 'GIRASOL')]
    CURRENCY_CHOICES = [('USA', 'Dolar USA'), ('ARG', 'Peso ARG')]

    numeroContrato = models.CharField(max_length=20, blank=True, default='', verbose_name='#Contrato')
    productor = models.ForeignKey(Productor, on_delete=models.CASCADE, blank=False, null=False)
    agente = models.ForeignKey(Agente, on_delete=models.SET_NULL, blank=False, null=True)
    toneladas = models.PositiveIntegerField(validators=[MinValueValidator(1)])
    fecha_inicio = models.DateField(auto_now=False, auto_now_add=False, verbose_name="Fecha de Inicio")
    fecha_fin = models.DateField(auto_now=False, auto_now_add=False, verbose_name="Fecha de Fin")
    cereal = models.CharField(max_length=2, choices=CEREAL_CHOICES, default='SJ')
    op_tipo = models.CharField(max_length=1, choices=OP_CHOICES, default='P', verbose_name="Tipo")
    moneda = models.CharField(max_length=3, choices=CURRENCY_CHOICES, default='USA', verbose_name="Moneda")
    precio = models.DecimalField(blank=True, null=True, max_digits=15, decimal_places=6,
                                 validators=[MinValueValidator(Decimal('0.00')),
                                            MaxValueValidator(Decimal(999999999.999999))])

    objects = models.Manager() # The default manager.
    alarma = OpAlarmManager()

    aproved = models.BooleanField(default=False, verbose_name = 'Apr')
    history = AuditlogHistoryField()

    def clean(self):
        if self.fecha_inicio > self.fecha_fin:
            raise ValidationError(
                _('La fecha de fin de la operación debe ser igual o posterior a la fecha fin del contrato.'))
        if self.op_tipo == self.A_PRECIO_OP and self.precio is None:
            raise ValidationError(_('Operaciones A Precio deben tener un valor fijado para precio.'))
        if self.op_tipo == self.A_FIJAR_OP and not self.precio is None:
            raise ValidationError(_('Operaciones A Fijar no pueden tener un valor fijado para precio.'))

    def __str__(self):
        return "Contrato %s con Productor %s - Cereal: %s" % (str(self.numeroContrato) if self.numeroContrato
        is not None else "S/N", self.productor.full_name if self.productor is not None else "Indefinido", self.nombre_cereal)

    @property
    def short_description(self):
        return "%s" % self

    @property
    def nombre_cereal(self):
        for k, v in self.CEREAL_CHOICES:
            if k==self.cereal:
                return v



    def status(self, agente):
        if not self.aproved:
            return C_NO_APPROVED
        today = ARGENTINA_TIME.localize(datetime.today()).date()
        liq = LiquidacionAPrecio.objects.filter(contrato__id=self.id) \
            if self.op_tipo == 'P' else LiquidacionAFijar.objects.filter(contrato__id=self.id)
        if(today > self.fecha_fin):
            # contato ended
            tons = (lambda x: x if x is not None else 0)(liq.aggregate(Sum('toneladas'))['toneladas__sum'])
            if liq.filter(estado__lt = 4 if self.op_tipo == 'P' else 5).count() > 0:
                return CENDED_WITH_WARNING_IN_LIQ # Warning en liquidaciones
            elif tons < self.toneladas:
                return CENDED_WITH_PE # No se entrego la cantidad de cereal acordado
            elif tons > self.toneladas:
                return CEND_WITH_SURPLUS # Se entrego mayor cantidad del cereal acordado. Es necesario definir excedente
            else:
                return CENDED_WITHOUT_ERRORS
        elif (self.fecha_fin - today).days < agente.user.userprofile.alarma0_expire_days and \
            ((lambda x: x if x is not None else 0)(liq.filter(estado__gte=2).aggregate(Sum('toneladas'))['toneladas__sum']) < self.toneladas):
            return CENDSOON_WITH_PE
        else:
            # contrato in progress
            toneladas_en_liquidaciones = 0
            for l in liq:
                toneladas_en_liquidaciones += l.toneladas
                if l.getAlarm(agente) != L_NON_ALARMA and l.getAlarm(agente) != L_FINALIZADA:
                    return CINPROGRESS_WITH_WARNING_IN_LIQ #Liquidaciones con alarmas
            if toneladas_en_liquidaciones > self.toneladas:
                return CINPROGRESS_WITH_SURPLUS # Hay mas toneladas en liquidaciones que las indicadas en el contrato
            else:
                # in progress without warnings
                return CINPROGRESS_WITHOUT_WARNING

class Liquidacion(models.Model):
    contrato = models.ForeignKey(Operacion, on_delete=models.CASCADE)
    toneladas = models.PositiveIntegerField(validators=[MinValueValidator(1)])
    fecha_cambio_estado = models.DateField(auto_now=False, auto_now_add=False, blank=True)
    numero_LPG = models.CharField(max_length=20, blank=True)
    tipo_de_cambio = models.DecimalField(max_digits=15, decimal_places=6, null=True, blank=True,
                                         validators=[MinValueValidator(Decimal('0.00')),
                                                     MaxValueValidator(Decimal(999999999.999999))])
    aproved = models.BooleanField(default=False, verbose_name='Apr')

    @abstractmethod
    def getAlarm(self, agente):
        pass

    @abstractmethod
    def satisfy_alarm(self, up, alarm):
        pass

    '''
    It returns the amount of liquidaciones whose state is "pendiente de liquidacion"
    and its fecha_cambio_estado is X days from today.
    '''
    def get_number_alarm_PL(agente):
        date = ARGENTINA_TIME.localize(datetime.today()).date() - timedelta(days=agente.user.userprofile.alarma_liq_pl)
        if agente.is_local_view:
            number = LiquidacionAPrecio.objects.filter(contrato__agente=agente, estado=2, fecha_cambio_estado__lt=date).count()
            number += LiquidacionAFijar.objects.filter(contrato__agente=agente, estado=3, fecha_cambio_estado__lt=date).count()
        else:
            number = LiquidacionAPrecio.objects.filter(estado=2, fecha_cambio_estado__lt=date).count()
            number += LiquidacionAFijar.objects.filter(estado=3, fecha_cambio_estado__lt=date).count()

        return number

    '''
    It returns the amount of liquidaciones whose state is "pendiente de liquidacion"
    and its fecha_cambio_estado is X days from today.
    '''
    def get_number_alarm_PP(agente):
        date = ARGENTINA_TIME.localize(datetime.today()).date() - timedelta(days=agente.user.userprofile.alarma_liq_pp)
        if agente.is_local_view:
            number = LiquidacionAPrecio.objects.filter(contrato__agente=agente, estado=3,
                                                       fecha_cambio_estado__lt=date).count()
            number += LiquidacionAFijar.objects.filter(contrato__agente=agente, estado=4,
                                                       fecha_cambio_estado__lt=date).count()
        else:
            number = LiquidacionAPrecio.objects.filter(estado=3, fecha_cambio_estado__lt=date).count()
            number += LiquidacionAFijar.objects.filter(estado=4, fecha_cambio_estado__lt=date).count()

        return number

    def get_number_alarm_PFP(agente):
        today = ARGENTINA_TIME.localize(datetime.today()).date()
        alarmEndDate = today + timedelta(days=agente.user.userprofile.alarma_liqPFP)
        if agente.is_local_view:
            number = LiquidacionAFijar.objects.filter(contrato__agente=agente, estado=2,
                                                      contrato__fecha_fin__range=[today, alarmEndDate]).count()
        else:
            number = LiquidacionAFijar.objects.filter(estado=2, contrato__fecha_fin__range=[today, alarmEndDate]).count()

        return number

    class Meta:
        abstract = True

class LiquidacionAPrecio(Liquidacion):
    ESTADO_CHOICE = [(STATUS_LIQ_A_PRECIO_PE, 'Pendiente de Entrega'),
                     (STATUS_LIQ_A_PRECIO_PL, 'Pendiente de Liquidación'),
                     (STATUS_LIQ_A_PRECIO_PP, 'Pendiente de Pago'),
                     (STATUS_LIQ_A_PRECIO_PC, 'Pagado a Cliente')]
    estado = models.IntegerField(choices=ESTADO_CHOICE, default='1')
    history = AuditlogHistoryField()

    @property
    def nombre_estado(self):
        return LiquidacionAPrecio.ESTADO_CHOICE[self.estado - 1][1]

    @property
    def tipo(self):
        return 'P'

    '''
    Este método brinda informacion para la columna de estado de la liquidación
    '''
    def getAlarm(self, agente):
        if not self.aproved:
            return L_NON_APPROVED

        today = ARGENTINA_TIME.localize(datetime.now()).date()

        if today < self.contrato.fecha_fin:
            if self.satisfy_alarm(agente.user.userprofile, ALARMA_PE):
                return ALARMA_PE

            elif self.satisfy_alarm(agente.user.userprofile, ALARMA_PL):
                return ALARMA_PL

            elif self.satisfy_alarm(agente.user.userprofile, ALARMA_PP):
                return ALARMA_PP

            elif self.estado == STATUS_LIQ_A_PRECIO_PC:
                return L_FINALIZADA

            else:
                return L_NON_ALARMA
        else:
            if self.estado == STATUS_LIQ_A_PRECIO_PE:
                return CONTRATO_FINALIZADO_PE

            if self.estado != STATUS_LIQ_A_PRECIO_PC:
                return CEND_AND_LIQ_IN_PROGRESS

            else:
                return L_FINALIZADA

    def satisfy_alarm(self, up, alarm):
        today = ARGENTINA_TIME.localize(datetime.now()).date()
        if alarm == ALARMA_PE:
            # Display ALARM_PE when contract is in progress
            return self.estado == STATUS_LIQ_A_PRECIO_PE and today < self.contrato.fecha_fin and (self.contrato.fecha_fin - today).days < up.alarma0_expire_days
        if alarm == ALARMA_PL:
            return self.estado == STATUS_LIQ_A_PRECIO_PL and (today - self.fecha_cambio_estado).days > up.alarma_liq_pl
        if alarm == ALARMA_PP:
            return self.estado == STATUS_LIQ_A_PRECIO_PP and (today - self.fecha_cambio_estado).days > up.alarma_liq_pp
        else:
            return False




class LiquidacionAFijar(Liquidacion):
    ESTADO_CHOICE = [(STATUS_LIQ_A_FIJAR_PE, 'Pendiente de Entrega'),
                     (STATUS_LIQ_A_FIJAR_PFP, 'Pendiente de Precio'),
                     (STATUS_LIQ_A_FIJAR_PL, 'Pendiente de Liquidación'),
                     (STATUS_LIQ_A_FIJAR_PP, 'Pendiente de Pago'),
                     (STATUS_LIQ_A_FIJAR_PC, 'Pagado a Cliente')]
    estado = models.IntegerField(choices=ESTADO_CHOICE, default='1')
    precioTonelada = models.DecimalField(blank=True, null=True, max_digits=15, decimal_places=6, verbose_name="Precio x Tonelada",
                                        validators = [MinValueValidator(Decimal('0.00')),
                                                      MaxValueValidator(Decimal(999999999.999999))])
    history = AuditlogHistoryField()

    @property
    def nombre_estado(self):
        return LiquidacionAFijar.ESTADO_CHOICE[self.estado - 1][1]

    @property
    def tipo(self):
        return 'F'

    '''
    Este método brinda informacion para la columna de estado de la liquidación
    '''

    def getAlarm(self, agente):
        if not self.aproved:
            return L_NON_APPROVED

        today = ARGENTINA_TIME.localize(datetime.now()).date()

        if today < self.contrato.fecha_fin:
            if self.satisfy_alarm(agente.user.userprofile, ALARMA_PE):
                return ALARMA_PE

            elif self.satisfy_alarm(agente.user.userprofile, ALARMA_PFP):
                return ALARMA_PFP

            elif self.satisfy_alarm(agente.user.userprofile, ALARMA_PL):
                return ALARMA_PL

            elif self.satisfy_alarm(agente.user.userprofile, ALARMA_PP):
                return ALARMA_PP

            elif self.estado == STATUS_LIQ_A_FIJAR_PC:
                return L_FINALIZADA

            else:
                return L_NON_ALARMA
        else:
            if self.estado == STATUS_LIQ_A_FIJAR_PE:
                return CONTRATO_FINALIZADO_PE

            if self.estado == STATUS_LIQ_A_FIJAR_PFP:
                return CONTRATO_FINALIZADO_PFP

            if self.estado != STATUS_LIQ_A_FIJAR_PC:
                return CEND_AND_LIQ_IN_PROGRESS

            else:
                return L_FINALIZADA



    def satisfy_alarm(self, up, alarm):
        today = ARGENTINA_TIME.localize(datetime.now()).date()

        if alarm == ALARMA_PE:
            # Display ALARM_PE when contract is in progress
            return self.estado == STATUS_LIQ_A_FIJAR_PE and today < self.contrato.fecha_fin and (self.contrato.fecha_fin - today).days < up.alarma0_expire_days
        if alarm == ALARMA_PFP:
            return (self.contrato.fecha_fin - today).days < up.alarma_liqPFP
        if alarm == ALARMA_PL:
            return self.estado == STATUS_LIQ_A_FIJAR_PL and (today - self.fecha_cambio_estado).days > up.alarma_liq_pl
        if alarm == ALARMA_PP:
            return self.estado == STATUS_LIQ_A_FIJAR_PP and (today - self.fecha_cambio_estado).days > up.alarma_liq_pp
        else:
            return False



auditlog.register(Operacion)
auditlog.register(LiquidacionAPrecio)
auditlog.register(LiquidacionAFijar)