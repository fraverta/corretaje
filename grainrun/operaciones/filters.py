import django_filters
from .models import Operacion, ALARMA_PL, ALARMA_PP, ALARMA_PFP, ALARMA_CONTRATO_PRONTO_A_VENCERSE, ALARMA_CONTRATO_VENCIDO_PE, ALARMA_CONTRATO_CON_EXCEDENTE
from .models import Agente

ALARM_CHOICES = [
                    (ALARMA_CONTRATO_PRONTO_A_VENCERSE, 'C. P a venc.'),
                    (ALARMA_CONTRATO_VENCIDO_PE, 'C. Vencidos PE'),
                    (ALARMA_CONTRATO_CON_EXCEDENTE, 'C. con Excedentes'),
                    (ALARMA_PFP, 'C. con liq PFP criticas.'),
                    (ALARMA_PL, 'C. con liq PL criticas.'),
                    (ALARMA_PP, 'C. con liq PP criticas.')
                ]

class AgenteFilter(django_filters.AllValuesFilter):

    @property
    def field(self):
        f = super(AgenteFilter, self).field
        f.choices = [('', '--------')] +  [(a.pk, a.full_name) for a in Agente.objects.all()]
        return f

class ProductorFilter(django_filters.ChoiceFilter):

    @property
    def field(self):
        self.extra['choices'] = [('', '--------')] + list(set([(op.productor.pk, op.productor.full_name) for op in self.parent.queryset]))
        return super(ProductorFilter, self).field


class GlobalOperacionFilter(django_filters.FilterSet):
    productor = ProductorFilter(label='Productor')
    agente = AgenteFilter(label='Agente')
    alarma = django_filters.ChoiceFilter(label='Alarmas', choices=ALARM_CHOICES, method='filter_alarm')
    #fecha_incio = django_filters.DateFromToRangeFilter(label='Fecha de Incio')
    #fecha_fin = django_filters.DateFromToRangeFilter(label='Fecha de Fin')



    def filter_alarm(self, queryset, name, value):
        value = int(value)
        if value == ALARMA_CONTRATO_PRONTO_A_VENCERSE:
            return queryset.filter(id__in = Operacion.alarma.get_alarm0(self.agente))
        elif value == ALARMA_CONTRATO_VENCIDO_PE:
            return queryset.filter(id__in = Operacion.alarma.get_alarm1(self.agente))
        elif value == ALARMA_PL:
            return queryset.filter(id__in = Operacion.alarma.get_alarmPL(self.agente))
        elif value == ALARMA_PP:
            return queryset.filter(id__in=Operacion.alarma.get_alarmPP(self.agente))
        elif value == ALARMA_PFP:
            return queryset.filter(id__in=Operacion.alarma.get_alarmPFP(self.agente))
        elif value == ALARMA_CONTRATO_CON_EXCEDENTE:
            return queryset.filter(id__in=Operacion.alarma.get_alarmCE(self.agente))

    def __init__(self, agente, *args, **kwargs):
        super(django_filters.FilterSet, self).__init__(*args, **kwargs)
        self.agente = agente

    class Meta:
        model = Operacion
        #fields = ['op_tipo','cereal', 'productor', 'agente','fecha_incio','fecha_fin']
        fields = ['op_tipo', 'cereal', 'productor', 'agente']


class LocalOperacionFilter(django_filters.FilterSet):
    productor = ProductorFilter(label='Productor')
    alarma = django_filters.ChoiceFilter(label='Alarmas', choices=ALARM_CHOICES, method='filter_alarm')
    #fecha_incio = django_filters.DateFromToRangeFilter(label='Fecha de Incio')
    #fecha_fin = django_filters.DateFromToRangeFilter(label='Fecha de Fin')

    def filter_alarm(self, queryset, name, value):
        value = int(value)
        if value == ALARMA_CONTRATO_PRONTO_A_VENCERSE:
            return queryset.filter(id__in = Operacion.alarma.get_alarm0(self.agente))
        elif value == ALARMA_CONTRATO_VENCIDO_PE:
            return queryset.filter(id__in = Operacion.alarma.get_alarm1(self.agente))
        elif value == ALARMA_PL:
            return queryset.filter(id__in = Operacion.alarma.get_alarmPL(self.agente))
        elif value == ALARMA_PP:
            return queryset.filter(id__in=Operacion.alarma.get_alarmPP(self.agente))
        elif value == ALARMA_PFP:
            return queryset.filter(id__in=Operacion.alarma.get_alarmPFP(self.agente))
        elif value == ALARMA_CONTRATO_CON_EXCEDENTE:
            return queryset.filter(id__in=Operacion.alarma.get_alarmCE(self.agente))

    def __init__(self, agente, *args, **kwargs):
        super(django_filters.FilterSet, self).__init__(*args, **kwargs)
        self.agente = agente

    class Meta:
        model = Operacion
        #fields = ['op_tipo','cereal', 'productor','fecha_incio','fecha_fin']
        fields = ['op_tipo','cereal', 'productor']



