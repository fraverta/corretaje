from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from .models import *
from django_tables2 import RequestConfig
from .filters import LocalOperacionFilter, GlobalOperacionFilter
from .tables import OperacionTable, OperacionTable_local_view
from .forms import OperacionForm, LiquidacionAPrecioForm, LiquidacionAFijarForm
from django.http import Http404
from django.contrib import messages
from django.views.decorators.cache import never_cache
import locale


NUM_OF_DECIMAL_TO_DISPLAY = 3

@login_required
def operacion_delete(request, pk):
    if request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser:
        operacion = get_object_or_404(Operacion, pk=pk)
        operacion.delete()
        messages.add_message(
            request, messages.SUCCESS, genDelOperacionInfoMsg(operacion),
            fail_silently=True,
        )
        return redirect('operacion_list')
    else:
        raise Http404



@login_required
@never_cache
def operacion_list(request):
    agente = Agente.objects.get(user__pk=request.user.pk)
    if request.user.groups.filter(name__in=['local_view']).exists():
        filter = LocalOperacionFilter(agente, request.GET, queryset=Operacion.objects.filter(agente__pk = agente.pk))
        table = OperacionTable_local_view(agente, filter.qs)
        liquidaciones = Operacion.objects.filter(agente__pk = agente.pk).values_list('pk', 'op_tipo')

    elif request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser:
        filter = GlobalOperacionFilter(agente, request.GET, queryset=Operacion.objects.all())
        table = OperacionTable(agente, filter.qs)
        liquidaciones = Operacion.objects.values_list('pk', 'op_tipo')

    else:
        raise Http404

    expanded = int(filter.data['alarma']) not in {ALARMA_CONTRATO_PRONTO_A_VENCERSE,ALARMA_CONTRATO_VENCIDO_PE,ALARMA_CONTRATO_CON_EXCEDENTE} \
                if 'alarma' in filter.data.keys() and filter.data['alarma'] != '' else False
    liq_data = {'parents':[]}
    locale.setlocale(locale.LC_ALL, 'es_AR.utf8')
    for c_id, tipo in liquidaciones:
        liq_list = []
        total = 0
        keys = ['id', 'tipo','toneladas', 'estado', 'fecha_cambio_estado', 'numero_LPG', 'monto', 'status']
        if Operacion.objects.get(pk=c_id).moneda == 'USA':
            keys.append('tipo_de_cambio')
            simbolo_moneda = 'USD '
        else:
            simbolo_moneda = '$ '
        if tipo == 'F':
           keys.append('precio')
        for l in LiquidacionAPrecio.objects.all().filter(contrato = c_id) if tipo=='P' else LiquidacionAFijar.objects.all().filter(contrato = c_id):

            liq = {'id':l.pk,'tipo':tipo,'toneladas': l.toneladas,'estado': l.nombre_estado,
                   'fecha_cambio_estado': l.fecha_cambio_estado.strftime('%d/%m/%Y'), 'numero_LPG':l.numero_LPG}

            liq['tipo_de_cambio'] = locale.currency(round(l.tipo_de_cambio, NUM_OF_DECIMAL_TO_DISPLAY), grouping=True) \
                                    if l.tipo_de_cambio is not None else 'A definir'

            if tipo == 'F':
                if l.precioTonelada is not None:
                    liq['precio'] = simbolo_moneda + locale.currency(round(l.precioTonelada, NUM_OF_DECIMAL_TO_DISPLAY),grouping=True)[2:]
                    if l.tipo_de_cambio is not None:
                        liq['monto'] = locale.currency(round(l.tipo_de_cambio * l.precioTonelada* Decimal(l.toneladas), NUM_OF_DECIMAL_TO_DISPLAY), grouping=True)
                    else:
                        liq['monto'] = "A definir"
                else:
                    liq['precio'] = "A definir"
                    liq['monto']  = "A definir"
            elif l.tipo_de_cambio is not None: #Tipo='P'
                liq['monto'] = locale.currency(round(l.tipo_de_cambio * Operacion.objects.get(pk=c_id).precio * Decimal(l.toneladas), 4), grouping=True)
            else:
                liq['monto'] = "A definir"

            liq['status'] = l.getAlarm(agente)

            # Calculo cantidad de dias para reportar en el tooltip del estado de la liquidacion
            # Hay 2 tipos de info:
            #   1) Respecto a la proximida al fin de contrato
            #   2) Respecto al ultimo dia de actualizacion
            today = ARGENTINA_TIME.localize(datetime.today()).date()
            if liq['status'] == ALARMA_PE or liq['status'] == ALARMA_PFP:
                liq["days"] = (l.contrato.fecha_fin - today).days
            else:
                liq["days"] = (today - l.fecha_cambio_estado).days




            liq['class'] = 'highlihted' if expanded and l.satisfy_alarm(agente.user.userprofile, int(filter.data['alarma'])) else ''
            liq['approved'] = str(l.aproved)
            liq_list.append(liq)
            total += l.toneladas
        contrato = Operacion.objects.get(pk=c_id)
        liq_data['parents'].append({'id':c_id,'tipo': tipo,'total':total,'restantes':contrato.toneladas - total, 'keys': keys ,'rows': liq_list})

    RequestConfig(request, paginate={'per_page': 25}).configure(table)
    data = dict(list({'filter': filter, 'table': table, 'view_name':'operacion_list','liq_data':liq_data, 'expanded': expanded}.items()))
    data['agente'] = agente
    return render(request, 'operaciones/operacion_list.html', data)


@login_required
def operacion_new(request):
    if request.method == "POST":
        form = OperacionForm(request.POST)
        if form.is_valid():
            operacion = form.save(commit=False)
            agente = Agente.objects.get(user=request.user.pk)
            if agente is None:
                print("Error in operation_new: Post agente is null!")
            operacion.agente = agente
            if request.user.groups.filter(name__in=['global_view']).exists():
                #Agents with global_view have their operations already aproved
                operacion.aproved = True
            operacion.save()
            if request.POST.get('_save'):
                messages.add_message(
                    request, messages.SUCCESS, genAddOpInfoMsg(operacion,request.user),
                    fail_silently=True,
                )
                return redirect('operacion_list')
            elif request.POST.get('_addanother'):
                messages.add_message(
                    request, messages.SUCCESS, genAddOpInfoMsg(operacion,request.user),
                    fail_silently=True,
                )
                return redirect('operacion_new')
            elif request.POST.get('_addLiquidacion'):
                messages.add_message(
                    request, messages.SUCCESS, genAddOpInfoMsg(operacion,request.user),
                    fail_silently=True,
                )
                return redirect('liquidacion_new', operacion_pk=operacion.pk)


    else:
        if request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser:
            form = OperacionForm()
        else:
            form = OperacionForm(initial={'restrict_prod_to': request.user.pk})
    return render(request, 'operaciones/operacion_new.html', {'form': form})

@login_required
def liquidacion_new(request, operacion_pk):
    op = get_object_or_404(Operacion, pk=operacion_pk)
    if request.user.agente.is_local_view and op.agente != request.user.agente:
        raise Http404
    else:
        if op.op_tipo == "P":
            return liquidacion_a_precio_new(request, op)
        else:
            return liquidacion_a_fijar_new(request, op)

def liquidacion_a_precio_new(request, op):
    if request.method == "POST":
        form = LiquidacionAPrecioForm(request.POST, contrato=op)
        if form.is_valid():
            liquidacion = form.save(commit=False)
            liquidacion.contrato = op
            if liquidacion.contrato.moneda == 'ARG':
                liquidacion.tipo_de_cambio = Decimal(1.0)
            if request.user.groups.filter(name__in=['global_view']).exists():
                #Agents with global_view have their operations already aproved
                liquidacion.aproved = True
            liquidacion.save()
            if request.POST.get('_save'):
                messages.add_message(
                    request, messages.SUCCESS, genAddLiquidacionInfoMsg(liquidacion),
                    fail_silently=True,
                )
                return redirect('operacion_list')
            elif request.POST.get('_addanother'):
                messages.add_message(
                    request, messages.SUCCESS, genAddLiquidacionInfoMsg(liquidacion),
                    fail_silently=True,
                )
                return redirect('liquidacion_new', operacion_pk=op.pk)
    else:
        data = {'contrato_':op}
        form = LiquidacionAPrecioForm(initial=data, contrato=op)

    return render(request, 'operaciones/liquidacion_new.html', {'form': form})

def liquidacion_a_fijar_new(request, op):
    if request.method == "POST":
        form = LiquidacionAFijarForm(request.POST, contrato=op)
        if form.is_valid():
            liquidacion = form.save(commit=False)
            liquidacion.contrato = op
            if liquidacion.contrato.moneda == 'ARG':
                liquidacion.tipo_de_cambio = Decimal(1.0)
            if request.user.groups.filter(name__in=['global_view']).exists():
                #Agents with global_view have their operations already aproved
                liquidacion.aproved = True
            liquidacion.save()
            if request.POST.get('_save'):
                messages.add_message(
                    request, messages.SUCCESS, genAddLiquidacionInfoMsg(liquidacion),
                    fail_silently=True,
                )
                return redirect('operacion_list')
            elif request.POST.get('_addanother'):
                form = LiquidacionAFijarForm(contrato=op)
                messages.add_message(
                    request, messages.SUCCESS, genAddLiquidacionInfoMsg(liquidacion),
                    fail_silently=True,
                )
                return redirect('liquidacion_new', operacion_pk=op.pk)
    else:
        data = {'contrato_':op}
        form = LiquidacionAFijarForm(initial=data, contrato=op)

    return render(request, 'operaciones/liquidacion_new.html', {'form': form})


@login_required
@never_cache
def operacion_edit(request, pk):
    #Only a global_view user can edit a page
    if request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser:
        operacion = get_object_or_404(Operacion, pk=pk)
        if request.method == "POST":
            form = OperacionForm(request.POST,instance=operacion)
            if form.is_valid():
                op = form.save(commit=False)
                if request.POST.get('_approve'):
                    op.aproved = True
                op.save()
                messages.add_message(
                    request, messages.SUCCESS, genEditOperacionInfoMsg(operacion),
                    fail_silently=True,
                )

                return redirect('operacion_list')
        else:
            form = OperacionForm(instance=operacion)
        to_be_approved = not operacion.aproved
        return render(request, 'operaciones/operacion_edit.html', {'form': form, 'to_be_approved':to_be_approved})
    else:
        raise Http404


@login_required
def operacion_detail(request, pk):
    operacion = get_object_or_404(Operacion, pk=pk)
    form = OperacionForm(instance=operacion, initial={'readonly': True})
    if 'agente' in form.fields:
        del form.fields['agente']

    if request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser:
        return render(request, 'operaciones/operacion_detail.html', {'form': form})
    elif request.user.groups.filter(name__in=['local_view']) and operacion.agente == Agente.objects.get(user__pk=request.user.pk):
        if not operacion.aproved:
            messages.add_message(
                request, messages.INFO, "Este contrato aún no ha sido aprobado.",
                fail_silently=True,
            )
        return render(request, 'operaciones/operacion_detail.html', {'form': form})
    else:
        raise Http404




@login_required
@never_cache
def liquidacion_edit_a_precio(request, pk):
    #Only a global_view user can edit a page
    liq = get_object_or_404(LiquidacionAPrecio, pk=pk)
    if request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser \
    or (request.user.groups.filter(name__in=['local_view']).exists() and liq.aproved and liq.contrato.agente.user == request.user):

        if request.method == "POST":
            form = LiquidacionAPrecioForm(request.POST, instance=liq, contrato=liq.contrato)
            if form.is_valid():
                liq = form.save(commit=False)
                if request.POST.get('_approve'):
                    liq.aproved = True
                if request.user.groups.filter(name__in=['local_view']).exists():
                    liq.aproved = False
                liq.save()
                messages.add_message(
                    request, messages.SUCCESS, genEditLiquidacionInfoMsg(liq),
                    fail_silently=True,
                )
                return redirect('operacion_list')
        else:
            data = {'contrato_': liq.contrato}
            form = LiquidacionAPrecioForm(instance=liq, initial=data, contrato=liq.contrato)
        to_be_approved = not liq.aproved
        return render(request, 'operaciones/liquidacion_edit.html', {'form': form, 'to_be_approved': to_be_approved})
    else:
        raise Http404

@login_required
@never_cache
def liquidacion_edit_a_fijar(request, pk):
    #Only a global_view user can edit a page or local when the liq has been approved
    liq = get_object_or_404(LiquidacionAFijar, pk=pk)
    if request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser\
        or (request.user.groups.filter(name__in=['local_view']).exists() and liq.aproved and liq.contrato.agente.user == request.user):
        if request.method == "POST":
            form = LiquidacionAFijarForm(request.POST, instance=liq, contrato=liq.contrato)
            if form.is_valid():
                liq = form.save(commit=False)
                if request.POST.get('_approve'):
                    liq.aproved = True
                if request.user.groups.filter(name__in=['local_view']).exists():
                    liq.aproved = False
                liq.save()
                messages.add_message(
                    request, messages.SUCCESS, genEditLiquidacionInfoMsg(liq),
                    fail_silently=True,
                )
                return redirect('operacion_list')
        else:
            data = {'contrato_': liq.contrato}
            form = LiquidacionAFijarForm(instance=liq, initial=data, contrato=liq.contrato)

        to_be_approved = not liq.aproved
        return render(request, 'operaciones/liquidacion_edit.html', {'form': form, 'to_be_approved': to_be_approved})
    else:
        raise Http404

@login_required
def liquidacion_delete(request, t, pk):
    if request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser:
        liq = get_object_or_404(LiquidacionAPrecio, pk=pk) if t == 'P' else get_object_or_404(LiquidacionAFijar, pk=pk)
        liq.delete()
        messages.add_message(
            request, messages.SUCCESS, genDelLiquidacionInfoMsg(liq),
            fail_silently=True,
        )
        return redirect('operacion_list')
    else:
        raise Http404

@login_required
def liquidacion_detail(request, t, pk):

    if t=='P':
        liq = get_object_or_404(LiquidacionAPrecio, pk=pk)
        form = LiquidacionAPrecioForm(instance=liq, initial={'readonly': True, 'contrato_':liq.contrato}, contrato=liq.contrato)
    else:
        liq = get_object_or_404(LiquidacionAFijar, pk=pk)
        form = LiquidacionAFijarForm(instance=liq,  initial={'readonly': True, 'contrato_':liq.contrato}, contrato=liq.contrato)

    if request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser:
        return render(request, 'operaciones/operacion_detail.html', {'form': form})
    elif request.user.groups.filter(name__in=['local_view']) and liq.contrato.agente == Agente.objects.get(user__pk=request.user.pk):
        if not liq.aproved:
            messages.add_message(
                request, messages.INFO, "Esta liquidación esta esperando aprobación, podrá moficarla cuándo sea aprobada.",
                fail_silently=True,
            )
        return render(request, 'operaciones/operacion_detail.html', {'form': form})
    else:
        raise Http404




def genAddOpInfoMsg(op,user):
    if user.groups.filter(name__in=['global_view']).exists() or user.is_superuser:
        msg = "La operacion <a href='/operacion/" + str(op.pk) + "/edit/'>"
    else:
        msg = "La operacion <a href='/operacion/" + str(op.pk) + "/detail/'>"

    msg += op.short_description + "</a> ha sido agregada."

    return msg


def genDelOperacionInfoMsg(op):
    msg = "La operación " + op.short_description + " ha sido eliminada"
    return msg

def genEditOperacionInfoMsg(op):
    msg = "El contrato <a href='/operacion/" + str(op.pk) + "/edit/'>"
    msg += op.short_description + "</a> ha sido actualizado"

    return msg

def genAddLiquidacionInfoMsg(liq):
    msg = "La <a href='/operacion/liquidacion/" + ("P" if liq.__class__ is LiquidacionAPrecio else "F") + "/"
    msg += str(liq.pk) + "/edit/'>" + "liquidación </a> ha sido agregada."
    return msg


def genEditLiquidacionInfoMsg(liq):
    msg = "La <a href='/operacion/liquidacion/" + ("P" if liq.__class__ is LiquidacionAPrecio else "F") + "/"
    msg += str(liq.pk) + "/edit/'>" + "liquidación </a> ha sido actualizada."
    return msg

def genDelLiquidacionInfoMsg(liq):
    msg = "Una liquidacion del " + liq.contrato.short_description + " ha sido eliminada."
    return msg