from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Productor(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    cuit = models.CharField(max_length=11,blank=True)
    localidad = models.CharField(max_length=50)
    calle = models.CharField(max_length=200,blank=True)
    numero = models.PositiveIntegerField(blank=True, null=True)
    telefono = models.CharField(max_length=30,blank=True, default="")
    mail = models.EmailField(blank=True)
    descripcion =  models.TextField(max_length=500,blank=True)
    agente = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

    @property
    def full_name(self):
        return '{}, {}'.format(self.apellido, self.nombre)

    def __iter__(self):
        for field in self._meta.get_fields():
            value = getattr(self, field.name, None)
            yield (field.verbose_name, value)


    def __str__(self):
        return self.full_name