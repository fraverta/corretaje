from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^productor/list/$', views.productor_list, name='productor_list'),
    url(r'^productor/new/$', views.productor_new, name='productor_new'),
    url(r'^productor/(?P<pk>\d+)/edit/$', views.productor_edit, name='productor_edit'),
    url(r'^productor/(?P<pk>\d+)/detail/$', views.productor_detail, name='productor_detail'),
    url(r'^productor/(?P<pk>\d+)/delete/$', views.productor_delete, name='productor_delete'),
]
