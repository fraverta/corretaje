from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from .models import Productor
from django_tables2 import RequestConfig
from productores.tables import ProductorTable, ProductorTable2
from productores.filters import ProductorFilter, ProductorFilter2
from .forms import ProductorForm,ProductorFormWithAgente
from django.http import Http404
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.contrib import messages
from django.views.decorators.cache import never_cache


@login_required
def productor_list(request):
    if request.user.groups.filter(name__in=['local_view']).exists():
        filter = ProductorFilter2(request.GET, queryset=Productor.objects.filter(agente=request.user))
        table = ProductorTable2(filter.qs)
    elif request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser:
        filter = ProductorFilter(request.GET, queryset=Productor.objects.all())
        table = ProductorTable(filter.qs)
    else:
        raise Http404

    #table = ProductorTable(Productor.objects.all())
    RequestConfig(request, paginate={'per_page': 25}).configure(table)
    return render(request, 'productores/productor_list.html',
                  dict(list({'filter': filter, 'table': table, 'view_name':'productor_list'}.items()) ))


@login_required
def productor_new(request):
    if request.method == "POST":
        form = ProductorForm(request.POST)
        if form.is_valid():
            productor = form.save(commit=False)
            productor.agente = request.user
            productor.save()
            if request.POST.get('_save'):
                messages.add_message(
                    request, messages.SUCCESS, genAddProdInfoMsg(productor, request.user),
                    fail_silently=True,
                )
                return redirect('productor_list')

            elif request.POST.get('_addanother'):
                messages.add_message(
                    request, messages.SUCCESS, genAddProdInfoMsg(productor, request.user),
                    fail_silently=True,
                )
                return redirect('productor_new')
    else:
        form = ProductorForm()
    return render(request, 'productores/productor_new.html', {'form': form})

@login_required
@never_cache
def productor_edit(request, pk):
    #Only a global_view user can edit a page
    if request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser:
        productor = get_object_or_404(Productor, pk=pk)
        if request.method == "POST":
            form = ProductorFormWithAgente(request.POST, instance=productor)
            if form.is_valid():
                productor = form.save(commit=False)
                try:
                    if form.cleaned_data.get('agentes') is "":
                        #No agent has been choosen
                        productor.agente = None
                    else:
                        user = User.objects.all().get(pk=form.cleaned_data.get('agentes'))
                        productor.agente = user
                except ObjectDoesNotExist as e:
                    print("ERROR on agentes.views.productor_edit: user doesn't exist on productor update")
                productor.save()
                messages.add_message(
                    request, messages.SUCCESS, genEditProdInfoMsg(productor, request.user),
                    fail_silently=True,
                )
                return redirect('productor_list')
        else:
            if(productor.agente):
                data =  {"agentes": productor.agente.pk}
            else:
                data = {"agentes": ""}
            form = ProductorFormWithAgente(instance=productor,initial=data)
        return render(request, 'productores/productor_edit.html', {'form': form, 'readonly':False})
    else:
        raise Http404


@login_required
def productor_detail(request, pk):
    productor = get_object_or_404(Productor, pk=pk)
    form = ProductorForm(instance=productor)
    return render(request, 'productores/productor_detail.html', {'form': form,'readonly':True})

@login_required
def productor_delete(request, pk):
    if request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser:
        productor = get_object_or_404(Productor, pk=pk)
        productor.delete()
        messages.add_message(
            request, messages.SUCCESS, genDelProdInfoMsg(productor),
            fail_silently=True,
        )
        return redirect('productor_list')
    else:
        raise Http404


def genAddProdInfoMsg(p,user):
    if user.groups.filter(name__in=['global_view']).exists() or user.is_superuser:
        msg = "El productor <a href='/productor/" + str(p.pk) + "/edit/'>"
    else:
        msg = "The productor <a href='/productor/" + str(p.pk) + "/detail/'>"

    msg += p.full_name + "</a> ha sido agregado"

    return msg

def genDelProdInfoMsg(p):
    msg = "El productor " + p.full_name + " ha sido eliminado"
    return msg

def genEditProdInfoMsg(p,user):
    msg = "El productor <a href='/productor/" + str(p.pk) + "/edit/'>"
    msg += p.full_name + "</a> ha sido actualizado"

    return msg