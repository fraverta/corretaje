import django_filters
from .models import Productor
from agentes.models import Agente
from django.db.models import Value
from django.db.models.functions import Concat

class ActuallyAllValuesFilter(django_filters.AllValuesFilter):

    @property
    def field(self):
        f = super(ActuallyAllValuesFilter, self).field
        f.choices =  [('', '--------')] + f.choices
        return f

class CityFilter(django_filters.ChoiceFilter):

    @property
    def field(self):
        self.extra['choices'] = [('', '--------')] + [(a.localidad, a.localidad) for a in self.parent.queryset]
        return super(CityFilter, self).field

class AgenteFilter(django_filters.AllValuesFilter):

    @property
    def field(self):
        f = super(AgenteFilter, self).field
        f.choices = [('', '--------')] +  [(a.user.pk, a.full_name) for a in Agente.objects.all()]
        return f

class ProductorFilter(django_filters.FilterSet):
    full_name = django_filters.CharFilter(label="Nombre", method='filter_fullName')
    localidad = CityFilter(label="Localidad")
    agente = AgenteFilter(label="Agente")
    class Meta:
        model = Productor
        fields = ['full_name','localidad','agente']


    def filter_fullName(self, queryset, name, value):
        return queryset.annotate(search_name=Concat('apellido', Value(', '), 'nombre')).filter(search_name__icontains=value)


#Filter productor without agent
class ProductorFilter2(django_filters.FilterSet):
    full_name = django_filters.CharFilter(label="Nombre",method='filter_fullName')
    localidad = CityFilter(label="Localidad")

    class Meta:
        model = Productor
        fields = ['full_name','localidad']


    def filter_fullName(self, queryset, name, value):
        return queryset.annotate(search_name=Concat('apellido', Value(', '), 'nombre')).filter(search_name__icontains=value)
