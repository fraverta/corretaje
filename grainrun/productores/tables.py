import django_tables2 as tables
from .models import Productor
from django.utils.safestring import mark_safe
from django.urls import reverse
from agentes.models import Agente
from django.core.exceptions import ObjectDoesNotExist

class FullNameColumn(tables.Column):
    def render(self, record):
        return record.apellido + ", " + record.nombre


class ProductorTable(tables.Table):
    full_name = tables.Column(verbose_name="Apellido, Nombre",order_by=('apellido', 'nombre'))
    agente = tables.TemplateColumn(" ")
    acciones = tables.TemplateColumn(verbose_name=("Acciones"), template_name='productores/my_column.html',
                                     orderable=False)

    def render_agente(self, record):
        try:
            agente = Agente.objects.get(user=record.agente)
            return mark_safe('<a href=' + reverse('agente_edit', args=[agente.pk]) + '>' + agente.full_name + '</a>')
        except ObjectDoesNotExist as e:
            return "—"

    class Meta:
        model = Productor
        fields = ('full_name','cuit', 'localidad', 'calle', 'numero', 'telefono', 'mail', 'agente')  # fields to display
        empty_text = "No hay productores que cumplan su criterio de búsqueda..."
        attrs = {"class": "table-striped table-bordered"}
        attrs = {'class': 'paleblue'}
        order_by = 'full_name'


class ProductorTable2(tables.Table):
    acciones = tables.TemplateColumn(verbose_name=("Acciones"), template_name='productores/my_column2.html',
                                     orderable=False)
    full_name = tables.Column(verbose_name="Apellido, Nombre",order_by=('apellido', 'nombre'))

    class Meta:
        model = Productor
        fields = ('full_name', 'cuit', 'localidad', 'calle', 'numero', 'telefono', 'mail')  # fields to display
        empty_text = "No hay productores que cumplan su criterio de búsqueda..."
        attrs = {"class": "table-striped table-bordered"}
        attrs = {'class': 'paleblue'}
        order_by = 'full_name'