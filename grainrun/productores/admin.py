from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Productor

# Register your models here.


class ProductorAdmin(admin.ModelAdmin):
    #list all fields from Productor in admin model
    list_display = [field.attname for field in Productor._meta.fields]

admin.site.register(Productor, ProductorAdmin)
