from django import forms
from .models import Productor
from agentes.models import Agente

EMPTY = "--------"

class ProductorForm(forms.ModelForm):
    class Meta:
        model = Productor
        fields = ('nombre', 'apellido', 'cuit', 'localidad', 'calle', 'numero', 'telefono', 'mail', 'descripcion')


class ProductorFormWithAgente(forms.ModelForm):
    agentes = forms.ChoiceField(required=False)

    def __init__(self, *args, **kwargs):
        super(ProductorFormWithAgente, self).__init__(*args, **kwargs)
        AGENTES_CHOICES = list(map(lambda a: (a["user"], a["apellido"] + ", " + a["nombre"]), Agente.objects.all().values('user', 'apellido', 'nombre')))
        AGENTES_CHOICES.append((None,EMPTY))
        self.fields['agentes'] = forms.ChoiceField(choices=AGENTES_CHOICES, label="Agente", required=False)

    class Meta:
        model = Productor
        fields = ('nombre', 'apellido', 'cuit', 'localidad', 'calle', 'numero', 'telefono', 'mail', 'agentes', 'descripcion')


