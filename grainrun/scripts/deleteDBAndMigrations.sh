#This Script deletes DB and all Migrations.
#After that create DB, superuser and give permisions.
#The project run again but without any data in DB

#!/bin/bash

PATH_TO_MANAGE="/home/nando/development/corretaje/grainrun"; #place in which manage.py is stored
ORIGINAL_PATH=$PWD;

cd $PATH_TO_MANAGE;
rm "db.sqlite3";
cd agentes/migrations/;
find . ! -name '__init__.py' -delete;
cd ../../managers/migrations/;
find . ! -name '__init__.py' -delete;
cd ../../operaciones/migrations/;
find . ! -name '__init__.py' -delete;
cd ../../productores/migrations/;
find . ! -name '__init__.py' -delete;
cd ../../;
echo "";
echo "***************************";
echo "Deleted DB and Migrations";
echo "***************************";
echo "";
python manage.py makemigrations;
python manage.py migrate;
./manage.py shell < scripts/createSuperUser.py;
./manage.py shell < scripts/permisions.py;
echo "";
echo "***********************************";
echo "Created SuperUser and Permissions";
echo "***********************************";
echo "";
cd $ORIGINAL_PATH;
