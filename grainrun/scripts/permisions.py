#Run this script into django shell (manage.py shell)
from django.contrib.auth.models import Group,Permission
from agentes.models import Agente
from django.contrib.auth.models import User

#Create local_view group
new_group, created = Group.objects.get_or_create(name='local_view')
prod_add_perm = Permission.objects.get(name='Can add productor')

#Create global_view group
new_group, created = Group.objects.get_or_create(name='global_view')

#An Agent that won't be listed and can not be eliminated
new_group, created = Group.objects.get_or_create(name='owner')

#Add owner permission to agente
u = User.objects.create_user('fdraverta@gmail.com', 'fdraverta@gmail.com', '123456', first_name='superagente',last_name='superagente')
Agente(user=u,nombre='superagente', apellido='superagente', localidad='admin', mail='fdraverta@gmail.com').save()
Agente.objects.get(mail='fdraverta@gmail.com')
Group.objects.get(name='global_view').user_set.add(Agente.objects.get(mail='fdraverta@gmail.com').user)
Group.objects.get(name='owner').user_set.add(Agente.objects.get(mail='fdraverta@gmail.com').user)