from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator

class Agente(models.Model):
    #on_delete cascade -> if a user is deleted then the agent linked to that user will be erased also
    #user has username,password,email,firstname and lastname attributes. That ones must not be redefined at Agente.
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=100, null=False, default='')
    apellido = models.CharField(max_length=100, null=False, default='')
    mail = models.EmailField(unique=True)
    cuit = models.CharField(max_length=11, blank=True)
    calle = models.CharField(max_length=200, blank=True)
    numero = models.PositiveIntegerField(blank=True, null=True)
    localidad = models.CharField(max_length=50)
    telefono = models.CharField(max_length=30, blank=True)
    descripcion =  models.TextField(max_length=500, blank=True)


    @property
    def full_name(self):
        return '{}, {}'.format(self.apellido, self.nombre)

    def __str__(self):
        return self.full_name

    @property
    def is_local_view(self):
        return self.user.groups.filter(name__in=['local_view']).exists()

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    alarma0_expire_days = models.PositiveIntegerField(blank=False, default=6, verbose_name='Alarma Contratos Prontos a Vencerse (en días)', validators=[MinValueValidator(1)])
    alarma_liq_pl = models.PositiveIntegerField(blank=False, default=10, verbose_name='Alarma Liquidaciones Pendientes de Liquidación (en días)',validators=[MinValueValidator(1)])
    alarma_liq_pp = models.PositiveIntegerField(blank=False, default=10, verbose_name='Alarma Liquidaciones Pendientes de Pago (en días)', validators=[MinValueValidator(1)])
    alarma_liqPFP = models.PositiveIntegerField(blank=False, default=10, verbose_name='Alarma Liquidaciones Pendientes de Fijación de Precios (en días)', validators=[MinValueValidator(1)])
