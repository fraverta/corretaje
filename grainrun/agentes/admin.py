from django.contrib import admin
from .models import Agente, UserProfile

# Register your models here.


class AgenteAdmin(admin.ModelAdmin):
    #list all fields from Productor in admin model
    list_display = [field.attname for field in Agente._meta.fields]

admin.site.register(Agente, AgenteAdmin)
admin.site.register(UserProfile)
