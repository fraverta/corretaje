from django import forms
from .models import Agente, UserProfile

class AgenteForm(forms.ModelForm):
    agente_choices = Agente.objects.all()
    permisos = forms.ChoiceField(choices=
                                 [('local_view', 'Vista de negocio local'),
                                  ('global_view', 'Vista de negocio global')])
    class Meta:
        model = Agente
        fields = ('nombre','apellido', 'cuit', 'localidad', 'calle', 'numero', 'telefono', 'mail', 'permisos', 'descripcion')



class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        exclude = ['user']
        fields = ['alarma0_expire_days', 'alarma_liqPFP', 'alarma_liq_pl', 'alarma_liq_pp']
