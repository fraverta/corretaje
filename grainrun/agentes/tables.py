import django_tables2 as tables
from .models import Agente

class AgenteTable(tables.Table):
    full_name = tables.Column(verbose_name="Apellido, Nombre",order_by=('apellido', 'nombre'))
    acciones = tables.TemplateColumn(verbose_name=("Acciones"), template_name='agentes/my_column.html',orderable=False)

    class Meta:
        model = Agente
        fields = ('full_name', 'cuit', 'localidad','calle','numero','telefono','mail','acciones') # fields to display
        empty_text = "No hay agentes que cumplan su criterio de búsqueda."
        attrs = {"class": "table-striped table-bordered"}
        attrs = {'class': 'paleblue'}
        order_by = 'full_name'

