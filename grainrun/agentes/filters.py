import django_filters
from .models import Agente
from django.db.models import Value
from django.db.models.functions import Concat

class ActuallyAllValuesFilter(django_filters.AllValuesFilter):

    @property
    def field(self):
        f = super(ActuallyAllValuesFilter, self).field
        f.choices =  f.choices + [('', 'Todos')]
        return f

class AgenteFilter(django_filters.FilterSet):
    full_name = django_filters.CharFilter(label="Nombre",method='filter_fullName')
    localidad = ActuallyAllValuesFilter(label="Localidad",)

    class Meta:
        model = Agente
        fields = ['full_name','localidad']

    def filter_fullName(self, queryset, name, value):
        return queryset.annotate(search_name=Concat('apellido', Value(', '), 'nombre')).filter(
            search_name__icontains=value)


