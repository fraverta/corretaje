from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from .forms import AgenteForm, UserProfileForm
from django.contrib.auth.models import User
from .models import Agente, UserProfile
from productores.models import Productor
from django_tables2 import RequestConfig
from .tables import AgenteTable
from .filters import AgenteFilter
from django.contrib.auth.models import Group
from django.http import Http404
from operaciones.models import Operacion
from django.contrib import messages
from django.urls import reverse
from django.views.decorators.cache import never_cache

DEFAULT_PASSWORD = "123456"

@login_required
def agente_new(request):
    if not(request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser):
        raise Http404

    if request.method == "POST":
        form = AgenteForm(request.POST)
        if form.is_valid():
            agente = form.save(commit=False)
            #we create a user with username=mail and password=DEFAULT_PASSWORD.
            user = User.objects.create_user(form.cleaned_data.get('mail'), form.cleaned_data.get('mail'), DEFAULT_PASSWORD,
                                            first_name=form.cleaned_data.get('nombre'), last_name=form.cleaned_data.get('apellido'))

            if form.cleaned_data.get('permisos') == 'local_view':
                g = Group.objects.get(name='local_view')
            else:
                g = Group.objects.get(name='global_view')

            g.user_set.add(user)

            setattr(agente, "user", user)
            agente.save()
            profile = UserProfile.objects.create(user=user)
            profile.user = user
            if request.POST.get('_save'):
                messages.add_message(
                    request, messages.SUCCESS, genAddAgenteInfoMsg(agente),
                    fail_silently=True,
                )
                return redirect('agente_list')
            elif request.POST.get('_addanother'):
                messages.add_message(
                    request, messages.SUCCESS, genAddAgenteInfoMsg(agente),
                    fail_silently=True,
                )
                return redirect(agente_new)

    else:
        form = AgenteForm()
    return render(request, 'agentes/agente_new.html', {'form': form})

@login_required
@never_cache
def agente_edit(request, pk):
    #Only a global_view user can edit a page
    if request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser:
        agente = get_object_or_404(Agente, pk=pk)
        if request.method == "POST":
            form = AgenteForm(request.POST,instance=agente)
            if form.is_valid():
                setattr(agente.user, "first_name", form.cleaned_data.get("nombre"))
                setattr(agente.user, "last_name", form.cleaned_data.get("apellido"))
                setattr(agente.user, "email", form.cleaned_data.get("mail"))
                setattr(agente.user, "username", form.cleaned_data.get("mail"))

                agente.user.groups.clear()
                if form.cleaned_data.get('permisos') == 'local_view':
                    g = Group.objects.get(name='local_view')
                else:
                    g = Group.objects.get(name='global_view')

                g.user_set.add(agente.user)

                agente.user.save()
                form.save(commit=True)
                messages.add_message(
                    request, messages.SUCCESS, genEditAgenteInfoMsg(agente),
                    fail_silently=True,
                )
                return redirect('agente_list')
        else:
            data = {'permisos': 'global_view'
                                if agente.user.groups.filter(name__in=['global_view']).exists() else
                                'local_view'
                    }
            form = AgenteForm(instance=agente,initial=data)
        return render(request, 'agentes/agente_edit.html', {'form': form})
    else:
        raise Http404


@login_required
def agente_list(request):
    if not(request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser):
        raise Http404
    #It list only agents who doesn't belong to owner group.
    filter = AgenteFilter(request.GET, queryset=Agente.objects.exclude(user__groups__name = 'owner'))
    table = AgenteTable(filter.qs)
    RequestConfig(request, paginate={'per_page': 25}).configure(table)
    return render(request, 'agentes/agente_list.html',
                  dict(list({'filter':filter, 'table': table, 'view_name':'agente_list'}.items())))

@login_required
def agente_delete(request, pk):
    if request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser:
        agente = get_object_or_404(Agente, pk=pk)

        # Check if it is not an owner (that kind of agents can not be elimnated)
        if agente.user.groups.filter(name__in=['owner']).exists():
            raise Http404
        else:
            #First, we update all agents operations as owner operation.
            owner = Agente.objects.filter(user__groups__name = 'owner')[0]
            if(owner is None):
                print("Error in agentes.models agente_delete: owner is null!.")

            # All productors which had been submmited by agent, now will be given to special user "owner"
            for p in Productor.objects.filter(agente=agente.user.pk):
                setattr(p, 'agente', owner.user)
                p.save()

            # All operations which had been submmited by agent, now will be given to special user "owner"
            for op in Operacion.objects.filter(agente__pk = pk):
                setattr(op, 'agente', owner)
                op.save()

            #remove user object related and agent (by delte cascade)
            user = get_object_or_404(User, pk=agente.user.pk)
            user.delete()
            messages.add_message(
                request, messages.SUCCESS, genDelAgenteInfoMsg(agente),
                fail_silently=True,
            )
            return redirect('agente_list')
    else:
        raise Http404


@login_required
@never_cache
def userprofile_edit(request):
    userprofile = get_object_or_404(UserProfile, pk=request.user)
    if request.method == "POST":
            form = UserProfileForm(request.POST,instance=userprofile)
            if form.is_valid():
                form.save(commit=True)
                messages.add_message(
                    request, messages.SUCCESS, genEditUserProfileInfoMsg(userprofile),
                    fail_silently=True,
                )
                return redirect('dashboard')
    else:
        form = UserProfileForm(instance=userprofile)
    return render(request, 'agentes/userprofile_edit.html', {'form': form})

def genAddAgenteInfoMsg(a):
    msg = "El agente <a href='" + reverse('agente_edit', args=(a.pk,)) + "'>" + a.full_name + "</a> ha sido agregado"
    return msg

def genDelAgenteInfoMsg(a):
    msg = "El agente " + a.full_name + " ha sido eliminado."
    return msg

def genEditAgenteInfoMsg(a):
    msg = "El agente <a href='" + reverse('agente_edit',args=(a.pk,)) + "'>" + a.full_name + "</a> ha sido actualizado"
    return msg

def genEditUserProfileInfoMsg(up):
    msg = "Sus <a href='" + reverse('userprofile_edit') + "'>preferencias </a> han sido actualizadas"
    return msg