from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^agente/list/$', views.agente_list, name='agente_list'),
    url(r'^agente/new/$', views.agente_new, name='agente_new'),
    url(r'^agente/(?P<pk>\d+)/edit/$', views.agente_edit, name='agente_edit'),
    url(r'^agente/(?P<pk>\d+)/delete/$', views.agente_delete, name='agente_delete'),
    url(r'^agente/userprofile/edit/$', views.userprofile_edit, name='userprofile_edit'),
]