from django.contrib.auth.decorators import login_required

# imports to send contact us email
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse
from django.shortcuts import render
from .forms import ContactForm
import json
from operaciones.models import Operacion, Liquidacion, LiquidacionAFijar, LiquidacionAPrecio
from agentes.models import Agente
from itertools import chain
from operator import attrgetter
from django.views.decorators.cache import never_cache

@login_required
@never_cache
def dashboard(request):
    info = {'a0': request.user.userprofile.alarma0_expire_days, 'a2':
            request.user.userprofile.alarma_liq_pl, 'a3':  request.user.userprofile.alarma_liq_pp,
            'a4':  request.user.userprofile.alarma_liqPFP}
    agente = Agente.objects.get(user__pk=request.user.pk)
    if request.user.groups.filter(name__in=['local_view']).exists():
        op_and_liq_to_be_aproved = sorted(chain(
                                        Operacion.objects.all().filter(agente=agente, aproved=False),
                                        LiquidacionAPrecio.objects.all().filter(contrato__agente=agente, aproved=False),
                                        LiquidacionAFijar.objects.all().filter(contrato__agente=agente, aproved=False)),
                                        key=lambda x: x.history.first().timestamp)
        return render(request, 'managers/dashboard.html', {'a0': len(Operacion.alarma.get_alarm0(agente)),
                                                            'a1': len(Operacion.alarma.get_alarm1(agente)),
                                                           'a2': Liquidacion.get_number_alarm_PL(agente),
                                                           'a3': Liquidacion.get_number_alarm_PFP(agente),
                                                           'a4': Liquidacion.get_number_alarm_PP(agente),
                                                           'a5': len(Operacion.alarma.get_alarmCE(agente)),
                                                           'info': info,
                                                           'op_and_liq_to_be_aproved':op_and_liq_to_be_aproved})
    elif request.user.groups.filter(name__in=['global_view']).exists() or request.user.is_superuser:
        op_and_liq_to_be_aproved = sorted(chain(
                                        Operacion.objects.all().filter(aproved=False),
                                        LiquidacionAPrecio.objects.all().filter(aproved=False),
                                        LiquidacionAFijar.objects.all().filter(aproved=False)),
                                        key=lambda x: x.history.first().timestamp)
        return render(request, 'managers/dashboard.html', {'a0': len(Operacion.alarma.get_alarm0(agente)),
                                                           'a1': len(Operacion.alarma.get_alarm1(agente)),
                                                           'a2': Liquidacion.get_number_alarm_PL(agente),
                                                           'a3': Liquidacion.get_number_alarm_PFP(agente),
                                                           'a4': Liquidacion.get_number_alarm_PP(agente),
                                                           'a5': len(Operacion.alarma.get_alarmCE(agente)),
                                                           'info':info,
                                                           'op_and_liq_to_be_aproved':op_and_liq_to_be_aproved})


#Render main web page with contact us form.
def mainPageView(request):
    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            subject = 'Consulta Sitio Web - %s' % (name)
            from_email = form.cleaned_data['email']
            message = "Mensaje de %s :\n \n" % (name) + form.cleaned_data['message'] + "\n\nResponder a: %s" % (
            from_email)
            try:
                send_mail(subject, message, from_email, ['mugica.corretajes@gmail.com'])
            except BadHeaderError:
                return HttpResponse(
                    json.dumps({"fail-msg": "Un error ha ocurrido. Por favor intente nuevamente más tarde."}),
                    content_type="application/json")
            return HttpResponse(
                json.dumps({"ok-msg": "Su mensaje ha sido enviado. Nos contactaremos con usted a la brevedad."})
                , content_type="application/json")
    return render(request, "index.html", {'form': form})
