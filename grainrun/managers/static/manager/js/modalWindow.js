var BstrapModal = function (title, body, buttonshtml) {
    buttons = buttons || [{
            Value: "CLOSE",
            Css: "btn-primary",
            Callback: function (event) {
                BstrapModal.Close();
            }
        }];
    var GetModalStructure = function () {
        var that = this;
        that.Id = BstrapModal.Id = Math.random();

        modal = '<div class="modal fade in" id="' + that.Id + '" role="dialog">';
        modal += '<div class="modal-dialog">';
        modal += '<div class="modal-content">';
        modal += '<div class="modal-header">';
        modal += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
        modal += '<h4 class="modal-title">' + title + '</h4>';
        modal += '</div>';
        modal += '<div class="modal-body">' + body + '</div>'
        modal += '<div class="modal-footer">' + buttonshtml + '</div>';
        modal += '</div>';
        modal += '</div>';
        modal += '</div>';

        return modal;
    }();

    BstrapModal.Delete = function () {
        var modals = document.getElementsByName("dynamiccustommodal");
        if (modals.length > 0) document.body.removeChild(modals[0]);
    };
    BstrapModal.Close = function () {
        $(document.getElementById(BstrapModal.Id)).modal('hide');
        BstrapModal.Delete();
    };
    this.Show = function () {
        BstrapModal.Delete();
        document.body.appendChild($(GetModalStructure)[0]);
        var btns = document.querySelectorAll("button[name='btn" + BstrapModal.Id + "']");
        for (var i = 0; i < btns.length; i++) {
            btns[i].addEventListener("click", buttons[i].Callback || BstrapModal.Close);
        }
        $(document.getElementById(BstrapModal.Id)).modal('show');
    };
};

var DeleteModelElemModal = function (modelName, body, deleteButtonURL) {

    buttons = '<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>'
    buttons += '<button onclick="location.href =\'' + deleteButtonURL + '\';"';
    buttons += 'type="button" class="btn btn-danger" data-dismiss="modal">Borrar</button>';

    return new BstrapModal("Borrar " + modelName, body, buttons );

};

var DeleteAgenteModal = function (nombre,deleteButtonURL) {
   console.log(deleteButtonURL);
   return new DeleteModelElemModal("Borrar Agente",
                            "Si continua se borrará el agente: <b>" + nombre  + "</b> de su lista de Agentes. Desea eliminarlo?",
                            deleteButtonURL).Show();

};

var DeleteProductorModal = function (nombre,deleteButtonURL) {
   console.log(deleteButtonURL);
   return new DeleteModelElemModal("Productor",
                            "Si continua se borrará el productor: <b>" + nombre  + "</b> de su lista de Productores " +
                            "y todas las operaciones que con él se hayan celebrado. Desea eliminarlo?",
                            deleteButtonURL).Show();

};

var DeleteOperacionModal = function (description,deleteButtonURL) {
   console.log(deleteButtonURL);
   return new DeleteModelElemModal("Contrato",
                            "Si continua se borrará el contrato: <b>" + description  + "</b> de su lista de contratos. Desea eliminarlo?",
                            deleteButtonURL).Show();

};

var DeleteLiquidacionModal = function (deleteButtonURL) {
   return new DeleteModelElemModal("Liquidación",
                            "Si continua se borrará la liquidación de su lista de contratos. Desea eliminarla?",
                            deleteButtonURL).Show();

};
