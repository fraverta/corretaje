from django.conf.urls import url
from . import views
from django_filters.views import object_filter

urlpatterns = [
    url(r'^$', views.mainPageView,name='home'),
    url(r'^dashboard/$',views.dashboard, name='dashboard'),
]


